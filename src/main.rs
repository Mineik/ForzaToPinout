mod format_service;

use clap::Parser;
use std::net::UdpSocket;
use core::time::Duration;
use serialport::{SerialPort, ClearBuffer};
use std::io::Read;
use crate::format_service::DataFormat;

#[derive(Parser)]
#[command()]
struct Params {
	#[arg(long)]
	pub stream_to_serial: bool,
	#[arg(long, short, default_value = "")]
	pub port_to_stream: String,
	#[arg(long)]
	pub test: bool,
}

fn main() -> std::io::Result<()> {
	let mut yet_to_clear_buffers = true;
	let params = Params::parse();
	let mut serial: Option<Box<dyn SerialPort>> = None;
	if params.stream_to_serial {
		serial = Some(serialport::new(params.port_to_stream, 115200).timeout(Duration::from_millis(2000)).open().expect("Falied to open port"));
		if params.test {
			let mut ser = serial.unwrap();
			ser.clear(ClearBuffer::All).expect("Couldnt clear bafrs!");
			ser.write(&40.88722f32.to_le_bytes()).expect("!!!");
			while ser.bytes_to_write()? > 0 {
				println!("{} bytes yet to write", ser.bytes_to_write()?)
			}
			std::thread::sleep(Duration::from_millis(10));
			print!("{} ->", 40.88722f32);
			for a in 40.88722f32.to_le_bytes() { print!(" {} ", a) };
			println!();
			println!("{}", ser.bytes_to_read()?);
			while ser.bytes_to_read()? > 0 {
				let mut xd: [u8; 4] = [0u8; 4];
				ser.read(&mut xd);

				print!("[ ");
				for a in xd { print!("{} ", a) }
				println!("]");
				println!("{}\n{}", f32::from_le_bytes(xd), f32::from_be_bytes(xd));
			}
			// quit after test
			return Ok(());
		}
	}

	let socket = UdpSocket::bind("127.0.0.1:8888")?;
	socket.connect("127.0.0.1:5200")?;
	// send to know we are connected here :)
	socket.send(&[0, 1, 2]).expect("TODO: panic message");
	println!("Bound to UDP socket!");
	loop {
		let mut buff: [u8; format_service::FORZA_PACKET_SIZE] = [0; format_service::FORZA_PACKET_SIZE];
		let _amt = socket.recv(&mut buff)?;

		let formatted = bincode::deserialize::<DataFormat>(&buff).unwrap();

		if !params.stream_to_serial {
			println!("Current speed: {:.2} m/s ({:.2} km/h), Current RPM: {:.0} ({})", formatted.speed_mps, formatted.speed_mps * 3.6, formatted.engine_current_rpm, formatted.gear);
			println!("-------------------------------------------------------")
		}

		if let Some(ref mut serial_conn) = serial {
			if yet_to_clear_buffers{
				serial_conn.clear(ClearBuffer::All)?;
				yet_to_clear_buffers = false;
			}
			let speed: f32 = formatted.speed_mps;
			// serial_conn.clear(ClearBuffer::Output);
			serial_conn.write(&speed.to_le_bytes())?;
			print!("{} ->", speed);
			for byte in speed.to_le_bytes() { print!(" {} ", byte) }
			print!(", ");
			serial_conn.flush();
			while serial_conn.bytes_to_write()? > 0 {
				std::thread::sleep(Duration::from_millis(5));
			}

			if serial_conn.bytes_to_read()? < 4 {println!();}
			while serial_conn.bytes_to_read()? >= 4 {
				let mut bufet = [0u8; 4];
				serial_conn.read(&mut bufet);
				for hamburger in bufet { print!(" {} ", hamburger); }
				println!("-> {}", f32::from_le_bytes(bufet));
			}
		}
	}
}
