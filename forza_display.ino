#include <TFT.h>  // Arduino LCD library
#include <SPI.h>

// pin definition for the Uno
#define cs   10
#define dc   9
#define rst  8

TFT TFTscreen = TFT(cs, dc, rst);

char sensorPrintout[4];

union le_flot {
	byte b[4];
	float fval;
};


float lastRecord = 0.0f;
byte bytes[4];


void displayBytesOnLEDs(byte byty[]) {
	for (int i = 0; i < 4; i++) {
		digitalWrite(2, bitRead(byty[i], 0));
		digitalWrite(3, bitRead(byty[i], 1));
		digitalWrite(4, bitRead(byty[i], 2));
		digitalWrite(5, bitRead(byty[i], 3));

		delay(500);

		digitalWrite(2, LOW);
		digitalWrite(3, LOW);
		digitalWrite(4, LOW);
		digitalWrite(5, LOW);

		delay(250);

		digitalWrite(2, bitRead(byty[i], 4));
		digitalWrite(3, bitRead(byty[i], 5));
		digitalWrite(4, bitRead(byty[i], 6));
		digitalWrite(5, bitRead(byty[i], 7));

		delay(500);

		digitalWrite(2, LOW);
		digitalWrite(3, LOW);
		digitalWrite(4, LOW);
		digitalWrite(5, LOW);

		delay(1000);
	}
}


void setup() {
	// Put this line at the beginning of every sketch that uses the GLCD:
	TFTscreen.begin();

	// clear the screen with a black background
	TFTscreen.background(0, 0, 0);

	// write the static text to the screen
	// set the font color to white
	TFTscreen.stroke(255, 255, 255);
	// set the font size
	TFTscreen.setTextSize(2);
	// write the text to the top left corner of the screen
	TFTscreen.text("Current speed :\n ", 0, 0);
	// ste the font size very large for the loop
	TFTscreen.setTextSize(5);

	Serial.begin(9600);

	pinMode(2, OUTPUT);
	pinMode(3, OUTPUT);
	pinMode(4, OUTPUT);
	pinMode(5, OUTPUT);

	pinMode(6, INPUT_PULLUP);


	digitalWrite(2, HIGH);
	digitalWrite(3, HIGH);
	digitalWrite(4, HIGH);
	digitalWrite(5, HIGH);

	delay(1000);

	digitalWrite(2, LOW);
	digitalWrite(3, LOW);
	digitalWrite(4, LOW);
	digitalWrite(5, LOW);

}

void loop() {
	// Read all data before writingm, but always at least 4 bytes should be available
	if (Serial.available() >= 4) {
		int rIndex = 0;
		while (Serial.available() > 0) {
			byte aaa = (byte) Serial.read();
			if (rIndex < 4) {
				bytes[rIndex] = aaa;
				rIndex++;
			} else {
				rIndex = 0;
			}
		}
		le_flot flot;

		flot.b[0] = bytes[0];
		flot.b[1] = bytes[1];
		flot.b[2] = bytes[2];
		flot.b[3] = bytes[3];

		float xdd = flot.fval;
		lastRecord = xdd;
		TFTscreen.stroke(0, 0, 0);
		TFTscreen.text(sensorPrintout, 0, 20);

		String spd = String(xdd * 3.6f);
		spd.toCharArray(sensorPrintout, 6);

		TFTscreen.stroke(0, 125, 255);
		TFTscreen.text(sensorPrintout, 0, 20);
	}

	if (!digitalRead(6)) {
		displayBytesOnLEDs(bytes);
	}
}